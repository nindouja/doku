import {Config} from './modules/Config.js'
import {SolverUI} from "./modules/SolverUI.js";


$(document).ready(function () {
    let $box = $(".box");

    Config.logger.info("Adding pencil marks placeholders in boxes");
    $box.load("box-content.html");

    Config.logger.info("Registering events handlers");
    $box
        .on("keypress", setInputKeyPress)
        .on("click", setInputClick);
});


/**
 * Set one input div from a keypress event on it
 * @param ev the keypress event
 */
const setInputKeyPress = function (ev) {
    setInput(ev.target, ev.key)
};

/**
 * Set one input div from a click event on it and then a user prompt
 * @param ev the click event
 */
const setInputClick = function (ev) {
    setInput(ev.target, prompt())
};

/**
 * Set one input div value if it's valid
 * @param el the input div
 * @param input the input value to set
 */
const setInput = function (el, input) {
    el = $(el);
    el = el.hasClass(".box") ? el : el.parent(".box");

    if (isNaN(input) || input < 1 || input > 9) {
        return
    }
    $(el).find(".final-value").text(input);

};


const loadConfig = function (el) {
    var c = {
        DELAY: $(el).find("input[name=delay]").val(),
        COMPUTED_COLOR: $(el).find("input[name=computedColor]").val(),
        CHANGED_COLOR: $(el).find("input[name=changedColor]").val(),
        UNCHANGED_COLOR: $(el).find("input[name=unchangedColor]").val()
    };

    for (var k in c) {
        if (c.hasOwnProperty(k)) {
            if (!c[k]) {
                delete c[k];
            }
            try {
                var tmp = parseInt(c[k]);
                if (!isNaN(tmp)) {
                    c[k] = tmp;
                }
            } catch (ignored) {
            }
        }
    }

    return c;
};

/**
 * Call the solver for the grid currently represented in the HTML
 */
window.solve = async function () {
    let solver = new SolverUI($('#main'), $("#debug-infos"), loadConfig($("#config")));
    await solver.load();
    await solver.solve();
    await solver.resetColors();
};


window.bigTest = async function () {
    confirm("Starting a million test ?");
    import('../resources/grids dataset/billionGrids.js').then(_test);
};
window.test = async function () {
    import('../resources/grids dataset/testGrids.js').then(_test);
};
const _test = async function (module) {

    const blockedGrid = 2;


    if (!module.grids) {
        alert("Data set not valid.")
    }
    let grids = module.grids;
    let i = 0;
    let stat = {success: 0, failure: 0};

    for (let gridConf of grids) {
        Config.logger.info("**************", `Starting grid #${i}`);
        Config.logger.info(gridConf);

        if (blockedGrid > -1 && blockedGrid !== i++) {
            Config.logger.info("Aborted");
            continue;
        }

        let solver = new SolverUI($('#main'), $("#debug-infos"), loadConfig($("#config")));
        await solver.clear();
        await solver.wait(1000);
        await solver.load(gridConf.grid);
        await solver.wait(1000);
        await solver.solve();

        if (solver.solved) {
            stat.success++;
            let soluce = solver.soluce;
            gridConf.soluce = soluce;
            Config.logger.info("--" + i + " solved");
            Config.logger.info(soluce);
        } else {
            stat.failure++;
            Config.logger.warn("--" + i + " not solved");
            Config.logger.info(solver.toString());
            Config.logger.info(solver.iterators);
            Config.logger.info(solver.lastMessage);
            debugger;
        }
        Config.logger.info(gridConf);
        await solver.wait(3000);
    }
    Config.logger.info("Finished tests", stat);
};
