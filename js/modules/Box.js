import {Config} from './Config.js'

export class Box {
    constructor(row, column, value) {
        this.row = row;
        this.column = column;
        this.finalValue = null;
        this.possibleValues = [];

        if (value > 0) {
            this.finalValue = parseInt(value);
        } else {
            this.possibleValues = Config.FULL_SET();
        }
    }

    getRow() {
        return this.row;
    }

    getColumn() {
        return this.column;
    }

    setFinalValue(value) {
        value = parseInt(value);
        if (value > 0) {
            this.finalValue = value;
            this.setPossibleValues(null);
        } else {
            this.finalValue = null;
        }
    }

    getFinalValue() {
        return this.finalValue
    }

    setPossibleValues(values) {
        if (values) {
            this.possibleValues = values;
            this.setFinalValue(null);
        } else {
            this.possibleValues = [];
        }
    }

    getPossibleValues() {
        return this.possibleValues
    }

    is(row, column) {
        return parseInt(row) === this.row && parseInt(column) === this.column
    }
}