import {Box} from "./Box.js";

export class BoxUI extends Box {
    constructor(row, column, value, boxDom, config) {
        super(row, column, value);
        this.boxDom = boxDom;
        this.config = config;

    }

    setFinalValue(value) {
        super.setFinalValue(value);
        if (this.finalValue > 0) {
            this.boxDom.find(".final-value").text(this.finalValue);
        } else {
            this.finalValue = null;
        }
    }

    setPossibleValues(values) {
        super.setPossibleValues(values);
        this.boxDom.find(".pm").removeClass("on");
        if (this.possibleValues) {
            for (let val of this.possibleValues) {
                this.boxDom.find(".pm-" + val).addClass("on");
            }

        } else {
            this.possibleValues = [];
        }
    }

    // Not overridden
    setComputing(){
        this.boxDom.addClass("computing");
    }

    setComputed(){
        this.boxDom.addClass("computed");
        this.boxDom.css("background-color",this.config.COMPUTED_COLOR);
    }

    setChanged(){
        this.boxDom.addClass("changed");
        this.boxDom.css("background-color",this.config.CHANGED_COLOR);
    }

    setUnchanged(){
        this.boxDom.addClass("unchanged");
        this.boxDom.css("background-color",this.config.UNCHANGED_COLOR);
    }

}