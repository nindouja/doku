import {Solver} from './Solver.js';
import {Util} from "./Util.js";
import {BoxUI} from "./BoxUI.js";
import {Config} from "./Config.js";


export class SolverUI extends Solver {


    constructor(gridDOM, debugDom, configOverride) {
        super();
        this.gridDom = $(gridDOM);
        this.debugDom = $(debugDom);
        this.config = {...Config, ...configOverride};

    }

    async load(def) {
        let UILoad = !def;
        Util.iterateSudoku(
            (r) => {
                this.grid[r] = this.grid[r] || [undefined];
            },
            ((c, r) => {
                    let dom = this.gridDom.find(this.select(r, c));
                    if (UILoad) {
                        let domValue = dom.find(" .final-value");
                        def += parseInt(domValue.text()) || 0;
                        domValue.text("");
                    }
                    this.grid[r][c] = new BoxUI(r, c, def[Util.flatIndex(r, c)], dom, this.config);
                    if (!UILoad) {
                        this.grid[r][c].setFinalValue(this.grid[r][c].getFinalValue())
                    }
                }
            ));
        await this.wait(this.config.DELAY * 2);
    }

    async onComputeBoxStart(r, c) {
        super.onComputeBoxStart(r, c);
        this.grid[r][c].setComputing();
        await this.wait(this.config.DELAY / 10)
    }

    async onComputeBoxEnd(r, c, status) {
        super.onComputeBoxEnd(r, c, status);

        // Log grid updates status
        if (status) {
            if (status.computed) {
                this.grid[r][c].setComputed();
            } else if (status.changed) {
                this.grid[r][c].setChanged();
            } else {
                this.grid[r][c].setUnchanged();
            }
        }

        // Log iterators
        this.generateStatDOM();

        await this.wait(this.config.DELAY);// Wait only if statuses are set; otherwise, just chang the color
        this.resetColors();
    }


    async notify() {
        super.notify();
        this.debugDom.find(".msg").text(this.__lastMessage);
    }

    async onIterationFinish() {
        this.generateStatDOM();
        super.onIterationFinish();
    }

    async onIterationStart() {
        this.generateStatDOM();
        super.onIterationStart();
    }

    async onCompletion() {
        this.generateStatDOM();
        super.onCompletion();
        this.resetColors(true);
    }


    // Not overridden
    async clear() {
        this.resetColors(true);
        this.gridDom.find(".box .final-value").text("");
        this.gridDom.find(".pm").removeClass("on");
    }

    select(row, column) {
        let sel = '';
        if (row) {
            sel += `.row-${row}`
        }
        if (column) {
            sel += `.col-${column}`
        }
        return sel;
    }

    async wait(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    resetColors(hard) {
        if (hard) this.gridDom.find(".box").removeClass("computed");
        this.gridDom.find(".box").removeClass("computing");
        this.gridDom.find(".box").removeClass("changed");
        this.gridDom.find(".box").removeClass("unchanged");
        this.gridDom.find(".box").css("background-color", "initial");
    }

    generateStatDOM() {
        let statsDom = this.debugDom.find("#stats");
        statsDom.empty();

        let iteratorsList = $("<ul>");
        for (let k1 in this._iterators) {
            if (this._iterators.hasOwnProperty(k1)) {
                let v1 = this._iterators[k1];
                let childEl;

                if (typeof v1 === 'object') {
                    childEl = $("<ul>");
                    for (let k2 in v1) {
                        if (v1.hasOwnProperty(k2)) {
                            let v2 = v1[k2];
                            childEl.append($("<li>")
                                .append($("<span>").text(k2))
                                .append(":")
                                .append($("<span>").text(v2)))
                        }
                    }
                } else {
                    childEl = $("<span>").text(v1);
                }

                iteratorsList.append(
                    $("<li>")
                        .append($("<span>").text(k1))
                        .append(":")
                        .append(childEl));
            }
        }

        let statList = $("<ul>");
        statList.append($("<li>").append($("<span>").text("Duration")).append(":").append($("<span>").text(((new Date().getTime() - this.start) / 1000) + "s")));

        return statsDom
            .append($("<h3>").text("Stats"))
            .append($("<fieldset>").append($("<legend>").text("General")).append(statList))
            .append($("<fieldset>").append($("<legend>").text("Iterations")).append(iteratorsList))
    }
}
