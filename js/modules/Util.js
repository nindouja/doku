export class Util {

    constructor() {
    }

    static iterateSudoku(onRowStart, onCol, onRowEnd) {
        for (let r = 1; r <= 9; r++) {
            if (typeof onRowStart === 'function') onRowStart(r);
            for (let c = 1; c <= 9; c++) {
                if (typeof onCol === 'function') onCol(c, r);
            }
            if (typeof onRowEnd === 'function') onRowEnd(r);
        }
    }
    static flatIndex(r, c) {
       return  ((r * 9) + c - 9) - 1;
    }
}
